<?php

namespace Certificationy\Web\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class NbAnswerValidator extends ConstraintValidator
{
    /**
     * 
     * @param Certificationy\Web\Entity\Doctrine\Answers $answers
     * @param Constraint $constraint
     */
    public function validate($answers, Constraint $constraint)
    {
        if ($answers->getQuestion()->isMultipleChoice() && $answers->countAnswers() < 2) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}