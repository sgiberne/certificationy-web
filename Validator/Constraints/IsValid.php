<?php

namespace Certificationy\Web\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsValid extends Constraint
{
    /* @todo verifier le texte */
    public $message = 'Wrong Response';
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}