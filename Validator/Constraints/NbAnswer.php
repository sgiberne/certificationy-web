<?php

namespace Certificationy\Web\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NbAnswer extends Constraint
{
    /* @todo verifier le texte */
    public $message = 'He must have at most two answers';
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}