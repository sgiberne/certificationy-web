<?php

namespace Certificationy\Web\Repository;

use Doctrine\ORM\EntityRepository;
use Certificationy\Web\Entity\Doctrine\Question;
use Doctrine\ORM\QueryBuilder;

/**
 * QuestionsAnswersRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class QuestionsAnswersRepository extends EntityRepository
{

    /**
     * Return all questions_answers by question
     * @param Question $question
     * @return QueryBuilder
     */
    public function findAllByQuestion(Question $question): QueryBuilder
    {
        return $this->createQueryBuilder('questions_answers')
                        ->andWhere('questions_answers.question = :question')
                        ->setParameter('question', $question);
    }

}
