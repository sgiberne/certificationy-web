<?php

namespace Certificationy\Web\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Certificationy\Web\Entity\Doctrine\QuestionsAnswers;

/**
 * AnswerQuestionsAnswers
 * @ORM\Table(name="answer")
 * @ORM\Entity(repositoryClass="Certificationy\Web\Repository\AnswerRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Answer
{

    /**
     * @var integer
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false)
     */
    private $label;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="QuestionsAnswers", mappedBy="answer", fetch="EAGER")
     */
    private $questionsAnswers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questionsAnswers = new ArrayCollection();
    }

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     * @param string $label
     * @return Answer
     */
    public function setLabel(string $label): Answer
    {
        $this->label = $label;
        return $this;
    }

    /**
     * Get label
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * Set updatedAt
     * @param \DateTime $updatedAt
     * @return Answer
     */
    public function setUpdatedAt(\DateTime $updatedAt): Answer
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     * @param \DateTime $createdAt
     * @return Answer
     */
    public function setCreatedAt(\DateTime $createdAt): Answer
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Get createdAt
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * Add questionsAnswer
     * @param QuestionsAnswers $questionsAnswer
     * @return Answer
     */
    public function addQuestionsAnswer(QuestionsAnswers $questionsAnswer): Answer
    {
        $this->questionsAnswers[] = $questionsAnswer;
        return $this;
    }

    /**
     * Remove questionsAnswer
     * @param QuestionsAnswers $questionsAnswer
     */
    public function removeQuestionsAnswer(QuestionsAnswers $questionsAnswer)
    {
        $this->questionsAnswers->removeElement($questionsAnswer);
    }

    /**
     * Get questionsAnswers
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionsAnswers(): Collection
    {
        return $this->questionsAnswers;
    }

}
