<?php

namespace Certificationy\Web\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Certificationy\Web\Entity\Doctrine\Category;
use Certificationy\Web\Entity\Doctrine\QuestionsAnswers;

/**
 * Question
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="Certificationy\Web\Repository\QuestionRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Question
{

    /**
     * @var integer
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false)
     */
    private $label;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $author;

    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $authorUrl;

    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $authorPicture;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \Certificationy\Web\Entity\Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="questions", fetch="EAGER")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="QuestionsAnswers", mappedBy="question")
     */
    private $questionsAnswers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questionsAnswers = new ArrayCollection();
    }

    /**
     * Get id
     * @return integer
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Set label
     * @param string $label
     * @return Question
     */
    public function setLabel(string $label): Question
    {
        $this->label = $label;
        return $this;
    }

    /**
     * Get label
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * Set author
     * @param string $author
     * @return Question
     */
    public function setAuthor(string $author): Question
    {
        $this->author = $author;
        return $this;
    }

    /**
     * Get author
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * Set author url
     * @param string $url
     * @return Question
     */
    public function setAuthorUrl(string $url): Question
    {
        $this->authorUrl = $url;
        return $this;
    }

    /**
     * Get author url
     * @return string
     */
    public function getAuthorUrl(): string
    {
        return $this->authorUrl;
    }

    /**
     * Set author picture
     * @param string $picture
     * @return Question
     */
    public function setAuthorPicture(string $picture): Question
    {
        $this->authorPicture = $picture;
        return $this;
    }

    /**
     * Get label
     * @return string
     */
    public function getAuthorPicture(): string
    {
        return $this->authorPicture;
    }

    /**
     * Set updatedAt
     * @param \DateTime $updatedAt
     * @return Question
     */
    public function setUpdatedAt(\DateTime $updatedAt): Question
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * Get updatedAt
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     * @param \DateTime $createdAt
     * @return Question
     */
    public function setCreatedAt(\DateTime $createdAt): Question
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Get createdAt
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * Set category
     * @param Category $category
     * @return Question
     */
    public function setCategory(Category $category = null): Question
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Get categories
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * Add questionsAnswer
     * @param QuestionsAnswers $questionsAnswer
     * @return Question
     */
    public function addQuestionsAnswer(QuestionsAnswers $questionsAnswer): Question
    {
        $this->questionsAnswers[] = $questionsAnswer;
        return $this;
    }

    /**
     * Remove questionsAnswer
     * @param QuestionsAnswers $questionsAnswer
     */
    public function removeQuestionsAnswer(QuestionsAnswers $questionsAnswer)
    {
        $this->questionsAnswers->removeElement($questionsAnswer);
    }

    /**
     * Get questionsAnswers
     * @return Collection
     */
    public function getQuestionsAnswers(): Collection
    {
        return $this->questionsAnswers;
    }

    /**
     * Check if a question have multiple answers
     * @return boolean
     */
    public function isMultipleAnswers(): bool
    {
        $nbValidAnswers = 0;
        foreach ($this->getQuestionsAnswers() as $questionAnswer) {
            if ($questionAnswer->getIsCorrect()) {
                $nbValidAnswers++;
            }

            if ($nbValidAnswers >= 2) {
                break;
            }
        }
        return $nbValidAnswers > 1;
    }

}
