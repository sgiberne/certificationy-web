<?php

namespace Certificationy\Web\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Certificationy\Web\Entity\Doctrine\Question;
use Certificationy\Web\Entity\Doctrine\Answer;

/**
 * QuestionsAnswers
 * @ORM\Table(name="questions_answers")
 * @ORM\Entity(repositoryClass="Certificationy\Web\Repository\QuestionsAnswersRepository")
 * @ORM\HasLifecycleCallbacks
 */
class QuestionsAnswers
{

    /**
     * @var boolean
     * @ORM\Column(name="is_correct", type="boolean", nullable=false)
     */
    private $isCorrect;

    /**
     * @var \Certificationy\Web\Entity\Doctrine\Question
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Question", inversedBy="questionsAnswers") 
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id", nullable=false)
     */
    private $question;

    /**
     * @var \Certificationy\Web\Entity\Doctrine\Answer
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Answer", inversedBy="questionsAnswers") 
     * @ORM\JoinColumn(name="answer_id", referencedColumnName="id", nullable=false)
     */
    private $answer;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * Set isCorrect
     * @param boolean $isCorrect
     * @return QuestionsAnswers
     */
    public function setIsCorrect(bool $isCorrect): QuestionsAnswers
    {
        $this->isCorrect = $isCorrect;
        return $this;
    }

    /**
     * Get isCorrect
     * @return boolean
     */
    public function getIsCorrect(): bool
    {
        return $this->isCorrect;
    }

    /**
     * Set question
     * @param Question $question
     * @return QuestionsAnswers
     */
    public function setQuestion(Question $question): QuestionsAnswers
    {
        $this->question = $question;
        return $this;
    }

    /**
     * Get question
     * @return Question
     */
    public function getQuestion(): Question
    {
        return $this->question;
    }

    /**
     * Set answer
     * @param Answer $answer
     * @return QuestionsAnswers
     */
    public function setAnswer(Answer $answer): QuestionsAnswers
    {
        $this->answer = $answer;
        return $this;
    }

    /**
     * Get answer
     * @return Answer
     */
    public function getAnswer(): Answer
    {
        return $this->answer;
    }

    /**
     * Set updatedAt
     * @param \DateTime $updatedAt
     * @return Answer
     */
    public function setUpdatedAt(\DateTime $updatedAt): QuestionsAnswers
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * Get updatedAt
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     * @param \DateTime $createdAt
     * @return Answer
     */
    public function setCreatedAt(\DateTime $createdAt): QuestionsAnswers
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Get createdAt
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

}
