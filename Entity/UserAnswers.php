<?php

namespace Certificationy\Web\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * UserAnswers
 */
class UserAnswers
{

    /**
     * @var Array
     * @Assert\NotBlank
     */
    private $answers;

    /**
     * Get userAnswers
     * @return array
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Set userAnswers
     * @param \Proxies\__CG__\Certificationy\Web\Entity\Answer|null $userAnswer
     * @return UserAnswers
     */
    public function setAnswers($userAnswer): UserAnswers
    {
        $this->answers = $userAnswer;
        return $this;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        $question    = $context->getRoot()->getConfig()->getAttributes()["data_collector/passed_options"]["question"];
        $userAnswers = $context->getValue()->getAnswers();

        if ($question->isMultipleAnswers() && (is_array($userAnswers) && count($userAnswers) < 2)) {
            $context->buildViolation('You must choose at least 2 answers.')
                    ->atPath('user_answers')
                    ->addViolation();
        }
    }

}
