<?php

namespace Certificationy\Web\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Certificationy\Web\Form\TestType;
use Certificationy\Web\Manager\QuestionSessionManager;
use Certificationy\Web\Manager\UserAnswersSessionManager;

class HomepageController extends Controller
{

    /**
     * @Route("/", name="homepage")
     * @Method({"GET", "POST"})
     */
    public function homeAction(Request $request)
    {
        $this->container->get(QuestionSessionManager::class)->resetQuestionIds();
        $this->container->get(UserAnswersSessionManager::class)->reset();

        $form = $this->createForm(TestType::class);

        if ($request->isMethod('POST')) {
            if ($form->handleRequest($request)->isValid()) {
                $categories = $form->get("category")->getData();
                $limit      = $form->get("limit")->getData();

                return $this->redirect($this->generateUrl("question_show", ["categories" => http_build_query($categories), "limit" => $limit]));
            }
        }

        return $this->render('CertificationyWebBundle:certificationy:homepage.html.twig', [
                    'form' => $form->createView(),
        ]);
    }

}
