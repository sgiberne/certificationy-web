<?php

namespace Certificationy\Web\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Certificationy\Web\Manager\QuestionManager;
use Certificationy\Web\Manager\UserAnswersSessionManager;
use Certificationy\Web\Manager\QuestionSessionManager;
use Certificationy\Web\Form\UserAnswersType;
use Certificationy\Web\Entity\UserAnswers;
use Exception;

class QuestionController extends Controller
{

    const DEFAULT_NB_QUESTIONS = 2;

    /**
     * @Route("/question/{categories}/{limit}", name="question_show", requirements={"categories": "[\d+=\d+&*]*", "limit": "\d+"})
     * @Method({"GET","POST"})
     */
    public function indexAction(Request $request, $categories = "", $limit = self::DEFAULT_NB_QUESTIONS)
    {
        // convert categories string to array
        parse_str($categories, $categories);

        $questionManager        = $this->container->get(QuestionManager::class);
        $questionSessionManager = $this->container->get(QuestionSessionManager::class);

        // init a set of question if we don't have one
        if (!$questionSessionManager->hasQuestionIds()) {
            $questionSessionManager->setQuestionIds($questionManager->getArrayOfRandomQuestionId($limit, $categories));
        }

        $id = $questionSessionManager->getCurrentQuestionId();

        if (!is_numeric($id)) {
            throw new Exception("An error occurred. Please restart test.");
        }

        $question = $questionManager->getRepository()->findOneById($id);

        $form = $this->createForm(UserAnswersType::class, new UserAnswers, ['question' => $question]);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $userAnswersSessionManager = $this->container->get(UserAnswersSessionManager::class);
                $userAnswersSessionManager->setUserAnswer($question, $form->getData());
                $questionSessionManager->next();

                return $this->redirect($this->generateUrl($questionSessionManager->countQuestionIds() === 0 ? "result_show" : "question_show"));
            }
        }

        return $this->render('CertificationyWebBundle:certificationy:question.html.twig', [
                    'base_dir'        => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
                    'form'            => $form->createView(),
                    'question'        => $question,
                    'left_questions'  => $questionSessionManager->countQuestionIds(),
                    'total_questions' => $questionSessionManager->totalQuestionIds(),
        ]);
    }

}
