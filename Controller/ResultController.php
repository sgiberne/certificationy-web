<?php

namespace Certificationy\Web\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Certificationy\Web\Manager\UserAnswersSessionManager;
use Certificationy\Web\Manager\QuestionManager;
use Certificationy\Web\Manager\QuestionSessionManager;

class ResultController extends Controller
{

    /**
     * @Route("/result", name="result_show")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $questionSessionManager    = $this->container->get(QuestionSessionManager::class);
        $userAnswersSessionManager = $this->container->get(UserAnswersSessionManager::class);

        $questionManager = $this->container->get(QuestionManager::class);
        $questions       = $questionManager->getRepository()->findBy(['id' => $questionSessionManager->getQuestionIds()]);
        $userAnswers     = $userAnswersSessionManager->getUserAnswers();

        return $this->render('CertificationyWebBundle:certificationy:result.html.twig', [
                    'user_answers' => $userAnswers,
                    'questions'    => $questions,
        ]);
    }

}
