<?php

namespace Certificationy\Web\Manager;

use Certificationy\Web\Manager\BaseManager;
use Certificationy\Web\Entity\Doctrine\Answer;
use \Certificationy\Web\Entity\Doctrine\Category;

class AnswerManager extends BaseManager
{

    public function addIfNotExist($label)
    {
        $answer = $this->getRepository()
                ->findOneByLabel($label);

        if (!$answer) {
            // create
            $answer = new Answer();
            $answer->setLabel($label)
                    ->setCreatedAt(new \DateTime())
                    ->setUpdatedAt(new \DateTime());

            $this->save($answer);
        }

        return $answer;
    }

    /**
     * Save Answer entity
     *
     * @param Answer $answer
     */
    public function save(Answer $answer)
    {
        $this->persistAndFlush($answer);
    }

    public function getRepository()
    {
        return $this->entityManager->getRepository('CertificationyWebBundle:Answer');
    }

}
