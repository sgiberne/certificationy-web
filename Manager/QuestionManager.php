<?php

namespace Certificationy\Web\Manager;

use Doctrine\ORM\EntityManager;
use Certificationy\Web\Manager\BaseManager;
use Certificationy\Web\Entity\Doctrine\Question;
use Certificationy\Web\Entity\Doctrine\Answer;
use Certificationy\Web\Entity\Doctrine\Category;

class QuestionManager extends BaseManager
{

    protected $entityManager;
    protected $questionAnswerManager;

    /**
     * 
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager, $questionAnswerManager)
    {
        parent::__construct($entityManager);
        $this->questionAnswerManager = $questionAnswerManager;
    }

    public function getArrayOfRandomQuestionId(int $limit = null, array $categories = []): array
    {
        $questions = $this->getRepository()->getRandomQuestion($limit, $categories);
        $ids       = [];

        foreach ($questions as $question) {
            $ids[] = $question->getId();
        }

        return $ids;
    }

    /**
     * 
     * @param Category $categorie
     * @param string $label
     * @param string $author
     * @param string $authorUrl
     * @param string $authorPicture
     * @return Question
     */
    public function addIfNotExist(Category $categorie, $label, $author = null, $authorUrl = null, $authorPicture = null)
    {
        $question = $this->getRepository()
                ->findOneByLabel($label);

        if (!$question) {
            // create
            $question = new Question();
            $question->setLabel($label)
                    ->setCategory($categorie)
                    ->setAuthor($author)
                    ->setAuthorPicture($authorPicture)
                    ->setAuthorUrl($authorUrl)
                    ->setCreatedAt(new \DateTime())
                    ->setUpdatedAt(new \DateTime());

            $this->save($question);
        }

        return $question;
    }

    public function addAnswerIfNotexist(Question $question, Answer $answer, bool $isCorrect)
    {
        return $this->questionAnswerManager->addIfNotexist($question, $answer, $isCorrect);
    }

    public function isMultiAnswers()
    {
        $answers = $this->getQuestionsAnswers();
        var_dump("toto", $answers);
        exit;
    }

    /**
     * Save Question entity
     *
     * @param Question $question
     */
    public function save(Question $question)
    {
        $this->persistAndFlush($question);
    }

    public function getRepository()
    {
        return $this->entityManager->getRepository('CertificationyWebBundle:Doctrine\Question');
    }

    public function getQuestionsAnswersRepository()
    {
        return $this->entityManager->getRepository('CertificationyWebBundle:Doctrine\QuestionsAnswers');
    }

}
