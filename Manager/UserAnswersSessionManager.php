<?php

namespace Certificationy\Web\Manager;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Certificationy\Web\Entity\UserAnswers;
use Certificationy\Web\Entity\Doctrine\Question;
use Doctrine\Common\Collections\ArrayCollection;

class UserAnswersSessionManager
{

    /** @var SessionInterface $session */
    private $session;

    /** @var string $userAnswersKey */
    private $userAnswersKey;

    /** @var ArrayCollection $userAnswers */
    private $userAnswers;

    public function __construct(string $sessionPrefix, SessionInterface $session)
    {
        $this->session        = $session;
        $this->userAnswersKey = $sessionPrefix . "userAnswers";
        $this->userAnswers    = [];
    }

    public function setUserAnswer(Question $question, UserAnswers $userAnswer): UserAnswersSessionManager
    {
        $this->userAnswers = $this->getUserAnswers();

        $this->userAnswers[] = ['question'    => $question,
            'user_answer' => $userAnswer];

        $this->session->set($this->userAnswersKey, $this->userAnswers);
        return $this;
    }

    public function getUserAnswers(): array
    {
        return $this->session->get($this->userAnswersKey, $this->userAnswers);
    }

    public function reset(): UserAnswersSessionManager
    {
        $this->session->set($this->userAnswersKey, new ArrayCollection);
        return $this;
    }

}
