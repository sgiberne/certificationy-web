<?php

namespace Certificationy\Web\Manager;

use Doctrine\ORM\EntityManager;
use Certificationy\Web\Manager\BaseManager;
use Certificationy\Web\Entity\Doctrine\QuestionsAnswers;
use Certificationy\Web\Entity\Doctrine\Question;
use Certificationy\Web\Entity\Doctrine\Answer;

class QuestionAnswerManager extends BaseManager
{

    public function findOneByPK($questionId, $answerId)
    {
        return $this->getRepository()->findOneBy(["question" => $questionId, "answer" => $answerId]);
    }

    public function addIfNotExist(Question $question, Answer $answer, bool $isCorrect)
    {
        $questionAnswer = $this->findOneByPK($question->getId(), $answer->getId());

        if (!$questionAnswer) {
            // create
            $questionAnswer = new QuestionsAnswers();
            $questionAnswer->setAnswer($answer)
                    ->setQuestion($question)
                    ->setIsCorrect($isCorrect)
                    ->setCreatedAt(new \DateTime())
                    ->setUpdatedAt(new \DateTime());

            $this->save($questionAnswer);
        }

        return $questionAnswer;
    }

    /**
     * Save QuestionAnswerquel entity
     *
     * @param QuestionsAnswers $questionAnswer
     */
    public function save(QuestionsAnswers $questionAnswer)
    {
        $this->persistAndFlush($questionAnswer);
    }

    public function getRepository()
    {
        return $this->entityManager->getRepository('CertificationyWebBundle:QuestionsAnswers');
    }

}
