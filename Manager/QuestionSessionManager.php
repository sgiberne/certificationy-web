<?php

namespace Certificationy\Web\Manager;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

class QuestionSessionManager
{

    /** @var SessionInterface $session */
    private $session;

    /** @var string $session */
    private $sessionQuestonsKey;

    /** @var string $session */
    private $sessionKey;

    public function __construct(string $sessionPrefix, SessionInterface $session)
    {
        $this->sessionQuestonsKey = $sessionPrefix . "questions";
        $this->sessionKey         = $sessionPrefix . "questions/key";
        $this->session            = $session;
    }

    /**
     * Increment key
     * @return \Certificationy\Web\Manager\QuestionSessionManager
     */
    public function next(): QuestionSessionManager
    {
        $key = $this->current() + 1;
        $this->setKey($key);
        return $this;
    }

    /**
     * return current key
     * @return int
     */
    private function current(): int
    {
        return $this->session->get($this->sessionKey, 0);
    }

    /**
     * Set key
     * @param int $key
     * @return \Certificationy\Web\Manager\QuestionSessionManager
     */
    private function setKey(int $key): QuestionSessionManager
    {
        $this->session->set($this->sessionKey, $key);
        return $this;
    }

    /**
     * Return question ids
     * @return array
     */
    public function getQuestionIds(): array
    {
        return $this->session->get($this->sessionQuestonsKey, []);
    }

    /**
     * Retrieve an questin id
     * @param int $key
     * @return array
     */
    public function getQuestionId(int $key): int
    {
        $questions = $this->session->get($this->sessionQuestonsKey, []);
        return array_key_exists($key, $questions) ? $questions[$key] : null;
    }

    /**
     * If question ids exist
     * @return bool
     */
    public function hasQuestionIds(): bool
    {
        return !empty($this->getQuestionIds());
    }

    /**
     * Set a list of question ids
     * @param array $ids
     * @return \Certificationy\Web\Manager\QuestionSessionManager
     */
    public function setQuestionIds(array $ids): QuestionSessionManager
    {
        $this->session->set($this->sessionQuestonsKey, $ids);
        return $this;
    }

    /**
     * Reset question ids
     * @return \Certificationy\Web\Manager\QuestionSessionManager
     */
    public function resetQuestionIds(): QuestionSessionManager
    {
        $this->session->set($this->sessionQuestonsKey, []);
        $this->session->set($this->sessionKey, 0);
        return $this;
    }

    /**
     * Return current question ids
     * @return int
     */
    public function getCurrentQuestionId(): int
    {
        $key = $this->current();
        return $this->getQuestionId($key);
    }

    /**
     * Return numbers of question ids left
     * @return int
     */
    public function countQuestionIds(): int
    {
        return $this->totalQuestionIds() - $this->current();
    }

    /**
     * Return total of question ids
     * @return int
     */
    public function totalQuestionIds(): int
    {
        return count($this->getQuestionIds());
    }

}
