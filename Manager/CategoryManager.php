<?php

namespace Certificationy\Web\Manager;

use Certificationy\Web\Manager\BaseManager;
use Certificationy\Web\Entity\Doctrine\Category;

class CategoryManager extends BaseManager
{

    public function addIfNotExist($name)
    {
        $category = $this->getRepository()
                ->findOneByName($name);

        if (!$category) {
            // create
            $category = new Category();
            $category->setName($name)
                    ->setCreatedAt(new \DateTime())
                    ->setUpdatedAt(new \DateTime());

            $this->save($category);
        }

        return $category;
    }

    /**
     * Save Category entity
     *
     * @param Category $category
     */
    public function save(Category $category)
    {
        $this->persistAndFlush($category);
    }

    public function getRepository()
    {
        return $this->entityManager->getRepository('CertificationyWebBundle:Category');
    }

}
