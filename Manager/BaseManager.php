<?php

namespace Certificationy\Web\Manager;

use Doctrine\ORM\EntityManager;

abstract class BaseManager
{

    protected $entityManager;

    /**
     * 
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    protected function persistAndFlush($entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

}
