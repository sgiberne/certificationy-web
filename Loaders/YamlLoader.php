<?php

namespace Certificationy\Web\Loaders;

use Certificationy\Loaders\YamlLoader as BaseLoader;
use Certificationy\Collections\Questions;
use Certificationy\Collections\Answers;
use Certificationy\Answer;
use Certificationy\Question;

class YamlLoader extends BaseLoader
{

    /**
     * @var Questions
     */
    private $questions;

    /**
     * @var string
     */
    private $paths;

    /**
     * @inheritdoc
     */
    public function loadAll(array $categories = []): Questions
    {
        $datas = $this->prepareFromYaml($categories, $this->paths);

        $questions = new Questions();

        foreach ($datas as $key => $data) {
            unset($datas[$key]);

            $answers = new Answers();

            foreach ($data['answers'] as $dataAnswer) {
                $answers->addAnswer(new Answer($dataAnswer['value'], $dataAnswer['correct']));
            }

            if (!isset($data['shuffle']) || true === $data['shuffle']) {
                $answers->shuffle();
            }

            $help = isset($data['help']) ? $data['help'] : null;

            $questions->add($key, new Question($data['question'], $data['category'], $answers, $help));
        }

        return $questions;
    }

    /**
     * @inheritdoc
     */
    public function retrieveById(int $key, array $categories): Questions
    {
        $data      = $this->prepareFromYaml($categories, $this->paths);
        $questions = new Questions();

        /* @todo corrige that */
        $item = $data[6];

        $answers = new Answers();

        foreach ($item['answers'] as $dataAnswer) {
            $answers->addAnswer(new Answer($dataAnswer['value'], $dataAnswer['correct']));
        }

        if (!isset($item['shuffle']) || true === $item['shuffle']) {
            $answers->shuffle();
        }

        $help = isset($item['help']) ? $item['help'] : null;

        $questions->add($key, new Question($item['question'], $item['category'], $answers, $help));

        return $questions;
    }

}
