<?php

namespace Certificationy\Web\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

class ExtractDataCommand extends Command
{

    protected function configure()
    {
        parent::configure();

        $this->setName('certificationy:extract:data');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $path        = $this->getApplication()->getKernel()->getContainer()->get('kernel')->getRootDir() . '/../data/tests';
        $destination = $path . '/data';

        $finder = new Finder();
        $finder->files()->in($path);
        $finder->files()->name('*.html')->depth(0);
        $cards  = [];

        foreach ($finder as $file) {
            $output->writeln($file->getRealPath() . " found");
            $content = $file->getContents();

            $crawler = new Crawler($content);

            $cards = $this->parseHtml($crawler);

            $this->saveData($this->prepareData($cards), $destination, $output);
        }
    }

    /**
     * 
     * @param Crawler $crawler
     * @return type
     */
    private function parseHtml(Crawler $crawler)
    {
        return $crawler->filter('.card')->each(function (Crawler $node, $i) {

                    $category = $node->filter('div.content:nth-child(1) > div.header')->each(function (Crawler $node, $i) {
                        preg_match("/[\w\s]+$/", trim($node->text()), $output);
                        return trim($output[0]);
                    });

                    $version = $node->filter('div.content:nth-child(1) > div:nth-child(1) > span')->each(function (Crawler $node) {
                        return trim(strip_tags($node->text()));
                    });

                    $title = $node->filter('.content')->eq(1)->each(function (Crawler $node, $i) {
                        return $node->filter('h2')->first()->text();
                    });

                    // question can be on some rows
                    $questionArray = $node->filter('div.content:nth-child(2) > h2')->nextAll()->each($this->getQuestionArray($node, $i));

                    $question = '';
                    foreach ($questionArray as $partOfQuestion) {
                        if (!empty($partOfQuestion)) {
                            $question .= $partOfQuestion;
                        }
                    }

                    if ($node->filter('div.content:nth-child(2) h3')->first()->text() == 'Correct') {
                        $goodAnswers = $node->filter('div.content:nth-child(2) h3:first-child ~ ul:nth-child(1) > li')->each(function (Crawler $node, $i) {
                            return trim(strip_tags($node->html(), '<code>'));
                        });
                    }

                    $answers = $node->filter('div.content:nth-child(2) > div:nth-child(5) > ul:nth-child(2) > li')->each($this->getAnswer($node, $goodAnswers));

                    $help = $node->filter('div.content:nth-child(2) > div:nth-child(6) > ul')->each(function (Crawler $node) {
                        return trim($node->text());
                    });

                    return [
                        'category' => $category[0],
                        'title'    => trim($title[0][0]),
                        'question' => trim($questionArray[0][0]),
                        'answers'  => $answers,
                        'version'  => $version,
                        'help'     => $help,
                    ];
                });
    }

    /**
     *
     * @param Crawler $node
     * @return string
     */
    protected function getQuestionArray(Crawler $node): string
    {
        $question = '';
        if (in_array($node->nodeName(), ['p', 'pre']) || ($node->nodeName() == 'div' && !is_null($node->attr('class')) && $node->attr('class') != 'prism-show-language')
        ) {
            $question .= trim(strip_tags($node->html(), '<code>'));
        }

        return $question;
    }

    /**
     * 
     * @param array $cards
     * @return type
     */
    private function prepareData(array $cards): array
    {
        $datas = [];
        foreach ($cards as $card) {
            // delete space
            $key = preg_replace('/\s+/', '', $card['category']);

            if (!array_key_exists($key, $datas)) {
                $datas[$key]['category'] = $card['category'];
            }
            unset($card['category']);
            $datas[$key]['questions'][] = $card;
        }
        return $datas;
    }

    /**
     *
     * @param Crawler $node
     * @param array $goodAnswers
     * @return array
     */
    protected function getAnswer(Crawler $node, array $goodAnswers): array
    {
        $answer = trim(strip_tags($node->html(), '<code>'));
        $correct = false;

        foreach ($goodAnswers as $goodAnswer) {
        $correct = ($goodAnswer == $answer);
        }

        return [
        'value' => $answer,
        'correct' => $correct,
        ];
    }

    /**
     * Save data into yml file
     * @param array $datas
     * @param string $path
     * @param OutputInterface $output
     */
    private function saveData(array $datas, string $path, OutputInterface $output)
    {
        foreach ($datas as $key => $data) {
            $yaml = Yaml::dump($data);
            $file = $path . '/' . $key . '.yaml';
            file_put_contents($file, $yaml);
            $output->writeln($file . " created");
        }
    }

}
