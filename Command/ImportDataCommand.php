<?php

namespace Certificationy\Web\Command;

use Certificationy\Web\Loaders\YamlLoader;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class StartCommand
 *
 * This is the command to start a new questions set
 */
class ImportDataCommand extends ContainerAwareCommand
{
    /**
     * @var integer
     */
    const WORDWRAP_NUMBER = 80;
    
    private $yamlLoader;
    
    public function __construct(YamlLoader $yamlLoader, $name = null)
    {
        parent::__construct($name);
        $this->yamlLoader = $yamlLoader;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('certificationy:import:data')
            ->setDescription('import data into database')
            ->addOption('categories', null, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Which categorie do you want import ? Default all', array())
            ->addOption('path', null, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'What is the path of your question files ?', array())
            ->addOption('reset', null, InputOption::VALUE_NONE, 'How many questions do you want?')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var \Certificationy\Web\Loaders\YamlLoader $loader */
        $loader = $this->getContainer()->get('certificationy.loader.yaml');
        var_dump($this->yamlLoader, $input->getOption('path')); exit;
        
        $questions = $loader->loadAll($input->getOption('categories')); // (nbQuestions, fromCategories)
        
        $traited = 1;
        
        foreach ($questions as $question) {
            // add category if not exist
            $categoryEntity = $this->getContainer()->get('certificationy.category_manager')->addIfNotExist($question->getCategory());
            
            // add question if not exist
            $questionManager = $this->getContainer()->get('certificationy.question_manager');
            $questionEntity = $this->getContainer()->get('certificationy.question_manager')->addIfNotExist($categoryEntity, $question->getQuestion());
            
            
            foreach ($question->getAnswers() as $answer) {
                $label = $answer->getValue();
                // add question if not exist
                $answerEntity = $this->getContainer()->get('certificationy.answer_manager')->addIfNotExist($label);
                $questionManager->addAnswerIfNotexist($questionEntity, $answerEntity, $answer->isCorrect());
            }
            
            $output->writeln(
                sprintf('<info>%s/%s</info> Question(s) traited', $traited, count($questions))
            );
            $traited++;
        }
    }
}

