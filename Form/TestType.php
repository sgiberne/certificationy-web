<?php

namespace Certificationy\Web\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Certificationy\Web\Entity\Doctrine\Category;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TestType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('category', EntityType::class, [
                    'class'        => Category::class,
                    'choice_label' => 'name',
                    'required'     => false,
                    'multiple'     => true,
                ])
                ->add('limit', ChoiceType::class, [
                    'choices'  => array(
                        10 => 10,
                        20 => 20,
                        40 => 40,
                    ),
                    'required' => true
        ]);
    }

}
