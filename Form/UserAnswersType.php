<?php

namespace Certificationy\Web\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Certificationy\Web\Entity\Doctrine\Answer;
use Certificationy\Web\Repository\AnswerRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserAnswersType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $question = $options['question'];

        $builder
                ->add('answers', EntityType::class, [
                    'class'         => Answer::class,
                    'query_builder' => function (AnswerRepository $answer) use ($question) {
                        return $answer->findByQuestionId($question->getId());
                    },
                    'choice_label' => 'label',
                    'label'        => $question->getLabel(),
                    'required'     => true,
                    'expanded'     => true,
                    'multiple'     => $question->isMultipleAnswers(),
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'question' => null,
        ));

        $resolver->setAllowedTypes('question', '\Certificationy\Web\Entity\Doctrine\Question');
    }

}
